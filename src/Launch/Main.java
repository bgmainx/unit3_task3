package Launch;

import entities.Menu;

/**
 * @author Borimir Georgiev
 */
public class Main
{
    public static void main(String[] args)
    {
        Menu.printContents();
    }
}

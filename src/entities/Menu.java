package entities;

import java.util.Scanner;

/**
 * @author Borimir Georgiev
 */
public abstract class Menu
{
    private static Scanner sc = new Scanner(System.in);
    private static final String ERR_MSG = "Please input a number.";

    public static void printContents()
    {
        System.out.println("Please choose an option:");
        System.out.println("[1] Calculate your car's fuel consumption per 100 km.");
        System.out.println("[2] Calculate the distance you'll travel with an N amount of fuel.");
        System.out.println("[3] Calculate the price of a trip.");
        System.out.println("[4] Calculate how many litres of fuel you'll need for a trip.");

        int userChoice = read();
        proceed(userChoice);
    }

    private static int read()
    {
        int userChoice = 0;

        // Checks if it's a number.
        try
        {
            userChoice = Integer.parseInt(sc.nextLine());
        } catch (NumberFormatException e)
        {
            System.out.println("Invalid choice! Input must be a number.");
            printContents();
            read();
        }

        // Checks if the list of options contains a number same as the users num.
        if (userChoice < 6)
        {
            return userChoice;

        } else
        {
            System.out.println("There isn't a choice with such number.");
            System.out.println("Please input a number between 1 and 5");
            printContents();
            return read();
        }
    }

    public static void proceed(int userChoice)
    {
        switch (userChoice)
        {
            case 1:
                calcFuelConsumption();
                printContents();
                break;
            case 2:
                calcExpectedDistance();
                printContents();
                break;
            case 3:
                calcPriceOfTrip();
                printContents();
                break;
            case 4:
                calcLitresNeeded();
                printContents();
                break;
        }
    }

    private static void calcLitresNeeded()
    {
        try
        {
            System.out.println("Please input the expected distance: ");
            double distance = Double.parseDouble(sc.nextLine());

            System.out.println("Please input expected fuel consumption:");
            double fuelConsumption = Double.parseDouble(sc.nextLine());

            System.out.printf("You'll need %.2f litres %n%n",
                    TripCalculator.getLitresNeeded(distance, fuelConsumption));

        } catch (NumberFormatException e)
        {
            System.out.println(ERR_MSG);
            System.out.println();
        }
    }

    private static void calcPriceOfTrip()
    {
        try {
            System.out.println("Please input the distance you want to travel:");
            double distance = Double.parseDouble(sc.nextLine());

            System.out.println("Please input expected fuel consumption:");
            double fuelConsumption = Double.parseDouble(sc.nextLine());

            System.out.println("Please input the fuel price:");
            double fuelPrice = Double.parseDouble(sc.nextLine());

            System.out.println("How many people will travel?");
            int people = Integer.parseInt(sc.nextLine());

            double price = TripCalculator.getPriceOfATrip(distance, fuelConsumption, fuelPrice, people);

            System.out.printf("Your trip will cost you %.2f lv. per person%n", price);
            System.out.printf("You will need %.0f litres of fuel. %n%n",
                    TripCalculator.getLitresNeeded(distance, fuelConsumption));
        } catch (NumberFormatException e)
        {
            System.out.println(ERR_MSG);
            System.out.println();
        }
    }

    private static void calcExpectedDistance()
    {
        try
        {
            System.out.println("How much fuel did you filled?");
            double litres = Double.parseDouble(sc.nextLine());

            System.out.println("How much litres of fuel per 100 km. does your car consume?");
            double fuelConsumption = Double.parseDouble(sc.nextLine());

            double expectedDistance = TripCalculator.getExpectedDistance(litres, fuelConsumption);

            System.out.printf("You'll travel %.2f km. %n%n", expectedDistance);

        } catch (NumberFormatException e)
        {
            System.out.println(ERR_MSG);
            System.out.println();
        }
    }

    private static void calcFuelConsumption()
    {
        try {
            System.out.println("Distance traveled:");
            double distance = Double.parseDouble(sc.nextLine());

            System.out.println("Litres of fuel used: ");
            double fuelLitresUsed = Double.parseDouble(sc.nextLine());

            double consumption = TripCalculator.getFuelConsumption(distance, fuelLitresUsed);

            System.out.printf("You car fuel consumption per 100 km. is %.2f L %n%n", consumption);
        } catch (NumberFormatException e)
        {
            System.out.println(ERR_MSG);
            System.out.println();
        }
    }


}

package entities;

/**
 * @author Borimir Georgiev
 */
public abstract class TripCalculator
{
    public static double getFuelConsumption(double distanceTraveled, double fuelUsed)
    {
        return 100 / (distanceTraveled / fuelUsed);
    }

    public static double getExpectedDistance(double litresFilled, double fuelConsumption)
    {
        return (litresFilled * 100) / fuelConsumption;
    }

    public static double getLitresNeeded(double distanceToTravel, double expectedFuelConsumption)
    {
        return (distanceToTravel / 100) * expectedFuelConsumption;
    }

    public static double getPriceOfATrip(double distanceToTravel, double expectedFuelConsumption,
                                         double fuelPrice, int persons)
    {
        return ((distanceToTravel / 100) * expectedFuelConsumption) * fuelPrice / persons;
    }

}
